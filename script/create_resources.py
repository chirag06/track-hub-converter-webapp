import requests
import json

def create_contents_txt(hub_link):
    url = "https://api.genome.ucsc.edu/list/hubGenomes?hubUrl="+hub_link
    res = requests.get(url)
    result = ""
    if res:
        res_json = json.loads(res.text)
        res_genomes = res_json["genomes"]

        for x in res_genomes:
            result += str(x) +"\t"+ str(res_genomes[x]['description'])+"\n"
    else:
        result = "404 error"
    return result

def create_genome_txt(hub_link, genome, assembly):
    url = "https://api.genome.ucsc.edu/list/chromosomes?hubUrl="+hub_link+";genome={};track={}".format(genome, assembly)
    res = requests.get(url)
    result = ""
    if res:
        res_json = json.loads(res.text)
        res_genomes = res_json["chromosomes"]

        for x in res_genomes:
            result += str(x) +"\t"+ str(res_genomes[x])+"\n"
    else:
        result = "404 error"
    return result
if __name__ == '__main__':
    contents = create_contents_txt("http://hgdownload.soe.ucsc.edu/hubs/mouseStrains/hub.txt")
    chromosomes = create_genome_txt("http://hgdownload.soe.ucsc.edu/hubs/mouseStrains/hub.txt", "CAST_EiJ", "assembly")
    print(contents)
    print("---------------------------------------------")
    print("---------------------------------------------")
    print(chromosomes)
